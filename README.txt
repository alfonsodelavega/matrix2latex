Takes a python or matlab matrix and outputs a latex table, a number of options are available. 
You can even use the module to output a latex matrix instead! 
The default table output is geared towards the standard recommended by IEEE, and uses the latex package booktabs. 
Check out the documentation for example output and usage.

This is an exported project from a Google Project repository. The original author is Øystein Bjørndal and the repository is at https://code.google.com/p/matrix2latex/
